.pragma library

function requestKey(host) {
  var xmlhttp, response;
  try {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "http://"+host+"/api", false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send("{ \"devicetype\": \"HuePlasmoid\" }");
    response = JSON.parse(xmlhttp.responseText)[0];
    if(response.error && response.error.type==101) {
      return 101;
    }
    if(response.success) {
      return response.success.username;
    }
    return false;
  } catch(error) {
    return false;
  };
}

function listLights(host, key) {
  var xmlhttp, response;
  try {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "http://"+host+"/api/"+key+"/lights", false);
    xmlhttp.send(null);
    response = JSON.parse(xmlhttp.responseText);
    return response;
  } catch(error) {
    return false;
  };
}

function listGroups(host, key) {
  var xmlhttp, response;
  try {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("GET", "http://"+host+"/api/"+key+"/groups", false);
    xmlhttp.send(null);
    response = JSON.parse(xmlhttp.responseText);
    return response;
  } catch(error) {
    return false;
  };
}

function setLightState(host, key, id, on, h, s, b, x, y) {
  var xmlhttp, response, state = {};
  if(on!==null)
    state.on = !!on;
  if(h!==null && 0 <= b <= 65535) {
    state.hue = Math.round(h);
  }
  if(s!==null && 0 <= s <= 254) {
    state.sat = Math.round(s);
  }
  if(b!==null && 0 <= b <= 254) {
    state.bri = Math.round(b);
  }
  if(x!==null && 0 <= x <= 1000 && y!==null && 0 <= y <= 1000) {
    state.xy = [Math.round(x) / 1000, Math.round(y) / 1000];
  }
  try {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("PUT", "http://"+host+"/api/"+key+"/lights/"+id+"/state", false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(state));
    response = JSON.parse(xmlhttp.responseText)[0];
    if(response.success) {
      return true;
    }
    return false;
  } catch(error) {
    return false;
  };
}

function setGroupState(host, key, id, on, h, s, b, x, y) {
  var xmlhttp, response, state = {};
  if(on!==null)
    state.on = !!on;
  if(h!==null && 0 <= h <= 254) {
    state.hue = Math.round(h);
  }
  if(s!==null && 0 <= s <= 254) {
    state.sat = Math.round(s);
  }
  if(b!==null && 0 <= b <= 254) {
    state.bri = Math.round(b);
  }
  if(x!==null && 0 <= x <= 1000 && y!==null && 0 <= y <= 1000) {
    state.xy = [Math.round(x) / 1000, Math.round(y) / 1000];
  }
  try {
    xmlhttp = new XMLHttpRequest();
    xmlhttp.open("PUT", "http://"+host+"/api/"+key+"/groups/"+id+"/action", false);
    xmlhttp.setRequestHeader("Content-Type", "application/json");
    xmlhttp.send(JSON.stringify(state));
    response = JSON.parse(xmlhttp.responseText)[0];
    if(response.success) {
      return true;
    }
    return false;
  } catch(error) {
    return false;
  };
}
