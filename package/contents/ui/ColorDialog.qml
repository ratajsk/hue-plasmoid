import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Dialogs 1.3 as Dialogs
import org.kde.kirigami 2.16 as Kirigami
import org.kde.plasma.components 3.0 as PC3

import "../code/requests.js" as Requests

Dialogs.Dialog {
  id: root

  property int chooseForLight: 0
  property int chooseForGroup: 0

  property int hValue: 0
  property int sValue: 0
  property int bValue: 0

  property int xValue: 0
  property int yValue: 0

  property int hValueNew: 0
  property int sValueNew: 0
  property int bValueNew: 0

  property int xValueNew: 0
  property int yValueNew: 0

  visible: false
  title: i18n("Choose light color")


  onVisibleChanged: {
    if(!visible)
      return (this.yValue = this.xValue = this.bValue = this.sValue = this.hValue = -1)

    this.hValueNew = this.hValue
    this.sValueNew = this.sValue
    this.bValueNew = this.bValue

    this.xValueNew = this.xValue
    this.yValueNew = this.yValue
  }

  onApply: {
    if(dialogContent.currentIndex==0 && (this.hValueNew!=this.hValue || this.sValueNew!=this.sValue || this.bValueNew!=this.bValue)) {
      if(chooseForLight > 0) {
        Requests.setLightState(plasmoid.configuration['host'], plasmoid.configuration['key'], chooseForLight, null, this.hValueNew, this.sValueNew, this.bValueNew, null, null);
      };
      if(chooseForGroup > 0) {
        Requests.setGroupState(plasmoid.configuration['host'], plasmoid.configuration['key'], chooseForGroup, null, this.hValueNew, this.sValueNew, this.bValueNew, null, null);
      };
      refresh();
    };
    if(dialogContent.currentIndex==1 && (this.xValueNew!=this.xValue || this.yValueNew!=this.yValue)) {
      if(chooseForLight > 0) {
        Requests.setLightState(plasmoid.configuration['host'], plasmoid.configuration['key'], chooseForLight, null, null, null, null, this.xValueNew, this.yValueNew);
      };
      if(chooseForGroup > 0) {
        Requests.setGroupState(plasmoid.configuration['host'], plasmoid.configuration['key'], chooseForGroup, null, null, null, null, this.xValueNew, this.yValueNew);
      };
      refresh();
    };
  }

  Connections {
    target: dialogFooter

    function onApplied() {
      close();
      apply();
    }
    function onRejected() {
      close();
      rejected();
    }
  }

  contentItem: Kirigami.Page {
    header: PC3.TabBar {
      id: dialogHeader

      anchors {
        left: parent.left
        right: parent.right
      }

      PC3.TabButton {
        text: i18n("HSB")
      }
      PC3.TabButton {
        text: i18n("CIE")
      }

      onCurrentIndexChanged: {
        dialogContent.reverseTransitions = this.currentIndex < dialogContent.currentIndex;
        dialogContent.currentIndex = this.currentIndex;
        switch(this.currentIndex) {
          case 0: dialogContent.replace(hsb); break;
          case 1: dialogContent.replace(cie);
        };
      }
    }

    contentItem: AnimatedStackView {
      id: dialogContent

      initialItem: hsb

      HSB {
        id: hsb
      }

      CIE {
        id: cie
      }
    }

    footer: Controls.DialogButtonBox {
      id: dialogFooter

      Controls.Button {
        id: dialogApplyButton

        Controls.DialogButtonBox.buttonRole: Controls.DialogButtonBox.ApplyRole

        icon.name: "dialog-ok-apply"
        text: i18n("Apply")
      }
      Controls.Button {
        id: dialogCancelButton

        Controls.DialogButtonBox.buttonRole: Controls.DialogButtonBox.RejectRole

        icon.name: "dialog-cancel"
        text: i18n("Cancel")
      }
    }
  }
}
