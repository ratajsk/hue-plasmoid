import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.16 as Kirigami

Component {
  id: hsb

  RowLayout {
    Kirigami.FormLayout {
      id: form

      Layout.fillWidth: true

      Controls.Slider {
        id: hSlider

        from: 0
        to: 65535

        Kirigami.FormData.label: i18n("Hue:")

        value: root.hValue

        onValueChanged: root.hValueNew = this.value

        Connections {
          target: root

          function onHValueChanged() {
            if(root.hValue >= 0)
              hSlider.value = root.hValue;
          }
        }
      }
      Controls.Slider {
        id: sSlider

        from: 0
        to: 254

        Kirigami.FormData.label: i18n("Saturation:")

        value: root.sValue

        onValueChanged: root.sValueNew = this.value

        Connections {
          target: root

          function onSValueChanged() {
            if(root.sValue >= 0)
              sSlider.value = root.sValue;
          }
        }
      }
      Controls.Slider {
        id: bSlider

        from: 0
        to: 254

        Kirigami.FormData.label: i18n("Brightness:")

        value: root.bValue

        onValueChanged: root.bValueNew = this.value

        Connections {
          target: root

          function onBValueChanged() {
            if(root.bValue >= 0)
              bSlider.value = root.bValue;
          }
        }
      }
    }

    Rectangle {
      Layout.fillHeight: true
      width: Kirigami.Units.gridUnit

      color: Qt.hsva(root.hValueNew / 65535, root.sValueNew / 254, 1, (root.bValueNew + 2) / 256)
    }
  }
}
