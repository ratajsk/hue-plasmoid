import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.16 as Kirigami

Component {
  id: cie

  Rectangle {

    width: dialogContent.width * 0.8
    height: dialogContent.width * 0.8

    color: "transparent"

    Image {
      source: "../res/CIE.png"

      width: dialogContent.width * 0.8
      height: dialogContent.width * 0.8
      fillMode: Image.PreserveAspectFit
    }

    Rectangle {
      id: cieHandle

      width: Kirigami.Units.gridUnit
      height: Kirigami.Units.gridUnit
      radius: dialogContent.width / 2

      color: "white"
      border {
        width: Kirigami.Units.gridUnit / 4

        color: "black"
      }

      rotation: 45

      x: root.xValue * dialogContent.width * 0.0008 - Kirigami.Units.gridUnit / 2
      y: dialogContent.width * 0.8 - root.yValue * dialogContent.width * 0.0008 - Kirigami.Units.gridUnit / 2

      MouseArea {
        anchors.fill: parent

        drag {
          target: parent
          axis: Drag.XAndYAxis
          minimumX: -Kirigami.Units.gridUnit
          maximumX: dialogContent.width * 0.8 + Kirigami.Units.gridUnit / 2
          minimumY: -Kirigami.Units.gridUnit
          maximumY: dialogContent.width * 0.8 + Kirigami.Units.gridUnit / 2
        }

        onReleased: {
          root.xValueNew = parent.x * 1250 / dialogContent.width - Kirigami.Units.gridUnit / 2;
          root.yValueNew = 1000 - parent.y * 1250 / dialogContent.width - Kirigami.Units.gridUnit / 2;
        }
      }

      Connections {
        target: root

        function onXValueChanged() {
          if(root.xValue >= 0) {
            cieHandle.x = root.xValue * dialogContent.width * 0.0008 - Kirigami.Units.gridUnit / 2;
          }
        }
        function onYValueChanged() {
          if(root.yValue >= 0) {
            cieHandle.y = dialogContent.width * 0.8 - root.yValue * dialogContent.width * 0.0008 - Kirigami.Units.gridUnit / 2;
          }
        }
      }
    }
  }
}
