import QtQuick 2.15
import QtQuick.Templates 2.15 as Templates
import org.kde.plasma.core 2.0 as PlasmaCore

Templates.StackView {
  id: stack

  property int currentIndex: 0
  property bool reverseTransitions: false
  popEnter: enterTransition
  popExit: exitTransition
  pushEnter: enterTransition
  pushExit: exitTransition
  replaceEnter: enterTransition
  replaceExit: exitTransition

  Transition {
    id: enterTransition

    NumberAnimation {
      properties: "x"
      from: (stack.reverseTransitions ? -0.5 : 0.5) * stack.width
      to: 0
      duration: PlasmaCore.Units.longDuration
      easing.type: Easing.InOutQuad
    }

    NumberAnimation {
      property: "opacity"
      from: 0.0
      to: 1.0
      duration: PlasmaCore.Units.longDuration
      easing.type: Easing.InOutQuad
    }
  }

  Transition {
    id: exitTransition

    NumberAnimation {
      properties: "x"
      from: 0
      to: (stack.reverseTransitions ? 0.5 : -0.5) * stack.width
      duration: PlasmaCore.Units.longDuration
      easing.type: Easing.InOutQuad
    }

    NumberAnimation {
      property: "opacity"
      from: 1.0
      to: 0.0
      duration: PlasmaCore.Units.longDuration
      easing.type: Easing.InOutQuad
    }
  }
}
