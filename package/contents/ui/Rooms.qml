import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import QtGraphicalEffects 1.0 as Effects
import org.kde.kirigami 2.16 as Kirigami

import "../code/requests.js" as Requests

Component {
  id: rooms

  Kirigami.CardsListView {
    model: roomsModel

    delegate: Kirigami.AbstractCard {
      layer.enabled: roomState && roomColor
      layer.effect: Effects.DropShadow {
        horizontalOffset: Kirigami.Units.gridUnit / 8
        verticalOffset: Kirigami.Units.gridUnit / 8

        color: Qt.hsva(roomColorH / 65535, roomColorS / 254, 1, (roomColorB + 2) / 512)
      }
      contentItem: Item {
        implicitWidth: roomsDelegateLayout.implicitWidth
        implicitHeight: roomsDelegateLayout.implicitHeight

        GridLayout {
          id: roomsDelegateLayout

          anchors {
            left: parent.left
            top: parent.top
            right: parent.right
          }
          columns: 3
          rowSpacing: Kirigami.Units.largeSpacing
          columnSpacing: Kirigami.Units.largeSpacing

          Kirigami.Heading {
            Layout.columnSpan: 3
            level: 2

            text: roomName
          }

          Controls.Button {
            id: roomButton

            checkable: true

            checked: roomState
            text: checked ? (roomState==1 ? i18n("Some on") : i18n("On")) : i18n("Off")

            onToggled: {
              Requests.setGroupState(plasmoid.configuration['host'], plasmoid.configuration['key'], roomId, this.checked, null, null, null, null, null);
              refresh();
            }
          }

          Controls.Button {
            id: roomColorButton

            visible: roomColor && roomState

            icon.name: "color-management"

            onClicked: {
              colorDialog.chooseForLight = 0;
              colorDialog.chooseForGroup = roomId;
              colorDialog.hValue = roomColorH;
              colorDialog.sValue = roomColorS;
              colorDialog.bValue = roomColorB;
              colorDialog.xValue = roomColorX * 1000;
              colorDialog.yValue = roomColorY * 1000;
              colorDialog.open();
            }
          }

          Controls.Slider {
            id: roomBrightnessSlider

            Layout.fillWidth: true

            from: 1
            to: 254

            visible: roomState
            value: roomColorB

            onPressedChanged: {
              if(pressed) {
                return;
              };
              Requests.setGroupState(plasmoid.configuration['host'], plasmoid.configuration['key'], roomId, null, null, null, this.value, null, null);
              refresh();
            }
          }
        }
      }
    }
  }
}
