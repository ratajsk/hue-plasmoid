import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import QtGraphicalEffects 1.0 as Effects
import org.kde.kirigami 2.16 as Kirigami

import "../code/requests.js" as Requests

Component {
  id: groups

  Kirigami.CardsListView {
    model: groupsModel

    delegate: Kirigami.AbstractCard {
      layer.enabled: groupState && groupColor
      layer.effect: Effects.DropShadow {
        horizontalOffset: Kirigami.Units.gridUnit / 8
        verticalOffset: Kirigami.Units.gridUnit / 8

        color: Qt.hsva(groupColorH / 65535, groupColorS / 254, 1, (groupColorB + 2) / 512)
      }

      contentItem: Item {
        implicitWidth: groupsDelegateLayout.implicitWidth
        implicitHeight: groupsDelegateLayout.implicitHeight

        GridLayout {
          id: groupsDelegateLayout

          anchors {
            left: parent.left
            top: parent.top
            right: parent.right
          }
          columns: 3
          rowSpacing: Kirigami.Units.largeSpacing
          columnSpacing: Kirigami.Units.largeSpacing

          Kirigami.Heading {
            Layout.columnSpan: 3
            level: 2

            text: groupName
          }

          Controls.Button {
            id: groupsButton

            checkable: true

            checked: groupState
            text: checked ? (groupState==1 ? i18n("Some on") : i18n("On")) : i18n("Off")

            onToggled: {
              Requests.setGroupState(plasmoid.configuration['host'], plasmoid.configuration['key'], groupId, this.checked, null, null, null, null, null);
              refresh();
            }
          }

          Controls.Button {
            id: groupColorButton

            visible: groupColor && groupState

            icon.name: "color-management"

            onClicked: {
              colorDialog.chooseForLight = 0;
              colorDialog.chooseForGroup = groupId;
              colorDialog.hValue = groupColorH;
              colorDialog.sValue = groupColorS;
              colorDialog.bValue = groupColorB;
              colorDialog.xValue = groupColorX * 1000;
              colorDialog.yValue = groupColorY * 1000;
              colorDialog.open();
            }
          }

          Controls.Slider {
            id: groupBrightnessSlider

            Layout.fillWidth: true

            from: 1
            to: 254

            visible: groupState
            value: groupColorB

            onPressedChanged: {
              if(pressed) {
                return;
              };
              Requests.setGroupState(plasmoid.configuration['host'], plasmoid.configuration['key'], groupId, null, null, null, this.value, null, null);
              refresh();
            }
          }
        }
      }
    }
  }
}
