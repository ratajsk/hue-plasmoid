import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.16 as Kirigami
import org.kde.kquickcontrolsaddons 2.0 as KQuickAddons

Kirigami.FormLayout {
  property alias cfg_icon: iconBtn.icon.name

  Layout.fillWidth: true

  Controls.Button {
    id: iconBtn

    icon.width: Kirigami.Units.gridUnit * 3
    icon.height: Kirigami.Units.gridUnit * 3

    Kirigami.FormData.label: i18n("Icon:")

    onClicked: {
      iconDialog.open()
    }
  }

  KQuickAddons.IconDialog {
    id: iconDialog

    onIconNameChanged: cfg_icon = iconName || "hue-plasmoid"
  }
}
