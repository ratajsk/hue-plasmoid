import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import QtGraphicalEffects 1.0 as Effects
import org.kde.kirigami 2.16 as Kirigami

import "../code/requests.js" as Requests

Component {
  id: lights

  Kirigami.CardsListView {
    model: lightsModel

    delegate: Kirigami.AbstractCard {
      layer.enabled: lightReachable && lightState && lightColor
      layer.effect: Effects.DropShadow {
        horizontalOffset: Kirigami.Units.gridUnit / 8
        verticalOffset: Kirigami.Units.gridUnit / 8

        color: Qt.hsva(lightColorH / 65535, lightColorS / 254, 1, (lightColorB + 2) / 512)
      }

      contentItem: Item {
        anchors {
          leftMargin: -Kirigami.Units.largeSpacing
          rightMargin: -Kirigami.Units.largeSpacing
          topMargin: -Kirigami.Units.largeSpacing * 2
          bottomMargin: -Kirigami.Units.largeSpacing * 2
        }
        implicitWidth: lightsDelegateLayout.implicitWidth
        implicitHeight: lightsDelegateLayout.implicitHeight

        GridLayout {
          id: lightsDelegateLayout

          anchors {
            fill: parent
            leftMargin: Kirigami.Units.largeSpacing
            rightMargin: Kirigami.Units.largeSpacing
            topMargin: Kirigami.Units.largeSpacing * 2
            bottomMargin: Kirigami.Units.largeSpacing * 2
          }
          columns: 3
          rowSpacing: Kirigami.Units.largeSpacing
          columnSpacing: Kirigami.Units.largeSpacing

          Kirigami.Heading {
            Layout.columnSpan: 3
            Layout.fillWidth: true

            level: 2

            text: lightName
          }

          Controls.Button {
            id: lightButton

            checkable: true

            checked: lightState
            text: checked ? i18n("On") : i18n("Off")

            onToggled: {
              Requests.setLightState(plasmoid.configuration['host'], plasmoid.configuration['key'], lightId, this.checked, null, null, null, null, null);
              refresh();
            }
          }

          Controls.Button {
            id: lightColorButton

            visible: lightReachable && lightColor && lightState

            icon.name: "color-management"

            onClicked: {
              colorDialog.chooseForLight = lightId;
              colorDialog.chooseForGroup = 0;
              colorDialog.hValue = lightColorH;
              colorDialog.sValue = lightColorS;
              colorDialog.bValue = lightColorB;
              colorDialog.xValue = lightColorX * 1000;
              colorDialog.yValue = lightColorY * 1000;
              colorDialog.open();
            }
          }

          Controls.Slider {
            id: lightBrightnessSlider

            Layout.fillWidth: true

            from: 1
            to: 254

            visible: lightReachable && lightState
            value: lightColorB

            onPressedChanged: {
              if(pressed) {
                return;
              };
              Requests.setLightState(plasmoid.configuration['host'], plasmoid.configuration['key'], lightId, null, null, null, this.value, null, null);
              refresh();
            }
          }

          Controls.Label {
            horizontalAlignment: Text.AlignLeft
            verticalAlignment: Text.AlignVCenter

            visible: !lightReachable
            color: "red"
            text: i18n("Light unreachable")
          }
        }
      }
    }
  }
}
