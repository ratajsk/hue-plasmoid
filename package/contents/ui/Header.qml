import QtQuick 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.16 as Kirigami
import org.kde.plasma.components 3.0 as PC3
import org.kde.plasma.extras 2.0 as PlasmaExtras

PlasmaExtras.PlasmoidHeading {
  id: header

  height: topRowLayout.implicitHeight + tabBar.implicitHeight

  RowLayout {
    id: topRowLayout

    anchors {
      left: parent.left
      right: parent.right
      leftMargin: Kirigami.Units.largeSpacing
      rightMargin: Kirigami.Units.largeSpacing
    }
    spacing: Kirigami.Units.largeSpacing

    Kirigami.Heading {
      id: heading

      Layout.fillWidth: true
      horizontalAlignment: Text.AlignLeft
      verticalAlignment: Text.AlignVCenter

      level: 1
      text: "Hue"
    }

    PC3.ToolButton {
      id: refreshButton

      Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

      icon.name: "view-refresh"

      enabled: plasmoid.configuration['host'].length > 0 && plasmoid.configuration['key'].length > 0
      onClicked: {
        refresh(true);
      }
    }

    PC3.ToolButton {
      id: configureButton

      Layout.alignment: Qt.AlignRight | Qt.AlignVCenter

      icon.name: "configure"

      onClicked: plasmoid.action("configure").trigger()
    }
  }

  PC3.TabBar {
    id: tabBar

    anchors {
      left: parent.left
      right: parent.right
      top: topRowLayout.bottom
    }

    PC3.TabButton {
      text: i18n("Lights")
    }
    PC3.TabButton {
      text: i18n("Groups")
    }
    PC3.TabButton {
      text: i18n("Rooms")
    }

    onCurrentIndexChanged: {
    //stack.reverseTransitions = (this.currentIndex==((stack.currentIndex + 2) % 3));
      stack.reverseTransitions = this.currentIndex < stack.currentIndex;
      stack.currentIndex = this.currentIndex;
      switch(this.currentIndex) {
        case 0: stack.replace(lights); break;
        case 1: stack.replace(groups); break;
        case 2: stack.replace(rooms);
      };
    }
  }
}
