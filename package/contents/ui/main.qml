import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Templates 2.15 as Templates
import org.kde.plasma.plasmoid 2.0

import "../code/requests.js" as Requests

Item {
  Plasmoid.icon: plasmoid.configuration.icon
  Plasmoid.preferredRepresentation: Plasmoid.compactRepresentation

  ListModel {
    id: lightsModel
  }
  ListModel {
    id: groupsModel
  }
  ListModel {
    id: roomsModel
  }

  function refresh(hard) {
    var response, x, y;

    if(!plasmoid.configuration['host'] || !plasmoid.configuration['key']) {
      return;
    };

    if(hard) {
      lightsModel.clear();
      groupsModel.clear();
      roomsModel.clear();
    };

    response = Requests.listLights(plasmoid.configuration['host'], plasmoid.configuration['key'])

    for(x = 1; response[x]; x++) {
      lightsModel.set(x - 1, {
        lightId: x,
        lightName: response[x].name,
        lightState: response[x].state.on,
        lightColor: !!response[x].state.colormode,
        lightColorH: response[x].state.colormode ? response[x].state.hue : 0,
        lightColorS: response[x].state.colormode ? response[x].state.sat : 0,
        lightColorB: response[x].state.colormode ? response[x].state.bri : 0,
        lightColorX: response[x].state.colormode ? response[x].state.xy[0] : 0,
        lightColorY: response[x].state.colormode ? response[x].state.xy[1] : 0,
        lightColorMode: response[x].state.colormode || "",
        lightReachable: response[x].state.reachable
      });
    };

    response = Requests.listGroups(plasmoid.configuration['host'], plasmoid.configuration['key']);

    for(x = 1; response[x]; x++) {
      if(response[x].type=="Room") {
        for(y = 0; y < roomsModel.count; y++) {
          if(roomsModel.get(y).roomId==x) {
            break;
          };
        };
        roomsModel.set(y, {
          roomId: x,
          roomName: response[x].name,
          roomState: response[x].state.all_on + response[x].state.any_on,
          roomColor: !!response[x].action.colormode,
          roomColorH: response[x].action.colormode ? response[x].action.hue : 0,
          roomColorS: response[x].action.colormode ? response[x].action.sat : 0,
          roomColorB: response[x].action.colormode ? response[x].action.bri : 0,
          roomColorX: response[x].action.colormode ? response[x].action.xy[0] : 0,
          roomColorY: response[x].action.colormode ? response[x].action.xy[1] : 0,
          roomColorMode: response[x].action.colormode || ""
        });
      }
      else {
        for(y = 0; y < groupsModel.count; y++) {
          if(groupsModel.get(y).groupId==x) {
            break;
          };
        };
        groupsModel.set(y, {
          groupId: x,
          groupName: response[x].name,
          groupState: response[x].state.all_on + response[x].state.any_on,
          groupColor: !!response[x].action.colormode,
          groupColorH: response[x].action.colormode ? response[x].action.hue : 0,
          groupColorS: response[x].action.colormode ? response[x].action.sat : 0,
          groupColorB: response[x].action.colormode ? response[x].action.bri : 0,
          groupColorX: response[x].action.colormode ? response[x].action.xy[0] : 0,
          groupColorY: response[x].action.colormode ? response[x].action.xy[1] : 0,
          groupColorMode: response[x].action.colormode || ""
        });
      };
    };
  }

  Plasmoid.fullRepresentation: Templates.Page {
    id: root

    header: Header {
      id: header
    }

    contentItem: AnimatedStackView {
      id: stack

      initialItem: lights

      Lights {
        id: lights
      }

      Groups {
        id: groups
      }

      Rooms {
        id: rooms
      }
    }

    ColorDialog {
      id: colorDialog
    }

    Component.onCompleted: {
      refresh();
    }
  }
}

