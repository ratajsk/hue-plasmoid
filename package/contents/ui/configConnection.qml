import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15 as Controls
import QtQuick.Dialogs 1.3 as Dialogs
import org.kde.kirigami 2.16 as Kirigami

import "../code/requests.js" as Requests

Kirigami.FormLayout {
  id: page

  Layout.fillWidth: true

  property alias cfg_host: host.text
  property alias cfg_key: key.text

  Controls.TextField {
    id: host

    Kirigami.FormData.label: i18n("Host:")
  }
  Controls.TextField {
    id: key

    Kirigami.FormData.label: i18n("Key:")
  }
  Controls.Button {
    id: request
  //icon.name: "entry-new"
    text: i18n("Request a new key")
    enabled: cfg_host.length > 0
    onClicked: {
      var key = Requests.requestKey(cfg_host);
      if(!key) {
        error.open();
      }
      else if(key===101) {
        youMustHoldTheButton.open();
      }
      else {
        cfg_key = key;
      }
    }
  }

  Dialogs.MessageDialog {
    id: error
    title: "Error"
    visible: false

    text: "Retrieving a new key resulted in failure, please check your host address."
  }
  Dialogs.MessageDialog {
    id: youMustHoldTheButton
    title: "Not authenticated"
    visible: false

    text: "Please hold the pairing button on your Bridge and try again."
  }
}
