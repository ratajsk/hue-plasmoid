#!/bin/sh
sudo cp ./package -Tr /usr/share/plasma/plasmoids/cz.ratajs.hue-plasmoid
sudo cp ./hue-plasmoid-breeze.svg -T /usr/share/icons/breeze/applets/48/hue-plasmoid.svg
sudo cp ./hue-plasmoid-breeze-dark.svg -T /usr/share/icons/breeze-dark/applets/48/hue-plasmoid.svg
sudo chmod 777 /usr/share/plasma/plasmoids/cz.ratajs.hue-plasmoid/contents/res/CIE.png
